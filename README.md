## Live version

A live version of this app can be found at http://pizza42.gildersleeve.eu/. The backing
API is at http://pizza42-api.gildersleeve.eu:8080/. They are hosted on two EC2 images.

## The walk-through

OK, the application doesn't do much. The only workflow is:

1. Go to landing page
1. Log in
1. Navigate to menu
1. Select pizzas
1. Submit your order

The final step is only possible if the account used to login with has a verified email address. In 
the background, the API is communicating with the Auth0 API and Google People API to gather various
information. The information of all users so gathered is visible from the API (no auth necessary) at

http://pizza42-api.gildersleeve.eu:8080/v1/allUsers

(Real secure, I know.)

More information about the API can be found at its Bitbucket repo: https://bitbucket.org/wgilder/pizza42-poc-api/src/master/

Thank you, and good night.

## Getting started

This is a small application demonstrating:

1. Auth0 authentication
1. Communicating with a custom API using JWT
1. Communicating with the Google API using OAuth

Build and run as expected:

```
npm install -g @angular/cli
npm install
ng serve --open
```

There are two environments, development and production. The development
environment uses as the web entry point 
```
http://localhost:4200/
```
and the API to be at
```
http://localhost:8080/v1/
```

The production environment (used when building the Docker image)
uses the following URLs:


```
http://pizza42.gildersleeve.eu/
http://pizza42-api.gildersleeve.eu:8080/v1
```

The application can be run as expected via `ng serve`, but is set up to be delivered via Docker. This
uses the Docker root image `tiangolo/node-frontend`, and using build stages most of the Node.js
libraries are removed with a thin NGINX server replacing it as web server.

## Creating the production Docker image

Install the Angular CLI and the dependencies for the app.

```
npm install -g @angular/cli
npm install

docker build -t pizza42-poc-spa .
docker login --username=$DOCKER_USERNAME
docker tag $IMAGE_ID wgilder/pizza42-poc-spa:$PUBLIC_TAG
docker push wgilder/pizza42-poc-spa
```

## Running Docker

The application is started using the following commands:

```
sudo docker pull wgilder/pizza42-poc-spa:initial
sudo docker run -ti --rm -d -p 80:80 --name pizza42_spa wgilder/pizza42-poc-spa:initial
```