export const environment = {
  production: true,
  api_url: 'http://pizza42-api.gildersleeve.eu:8080',
  callback_url: 'http://pizza42.gildersleeve.eu/callback'
};
