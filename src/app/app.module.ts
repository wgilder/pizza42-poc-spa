import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { CallbackComponent } from './callback/callback.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { PizzaMenuComponent } from './pizza-menu/pizza-menu.component';
import { HttpClientModule }    from '@angular/common/http';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    CallbackComponent,
    HeaderComponent,
    HomeComponent,
    PizzaMenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [
            'localhost:8080',
            'pizza42-api-dev.gildersleeve.eu:8080',
            'pizza42-api.gildersleeve.eu:8080',
        ],
        blacklistedRoutes: [ ]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
