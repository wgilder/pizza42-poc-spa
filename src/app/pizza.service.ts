import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Response } from '@angular/http';
import { Pizza } from './pizza';
import { AUTH_CONFIG } from './auth0-variables';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {
  
  private pizzasUrl = AUTH_CONFIG.apiURL + '/v1/allItems';
  private submitUrl = AUTH_CONFIG.apiURL + '/v1/submitOrder';
  private orderedPizzas: Pizza[] = [];
  private totalAmount: number = 0;
  
  getPizzas(): Observable<Pizza[]> {
    return this.http.get<Pizza[]>(this.pizzasUrl);
  }

  addToOrder(pizza): void {
    this.orderedPizzas.push(pizza);
    this.totalAmount = this.totalAmount + pizza.price;
  }

  removeFromOrder(pizza): void {
    var looking = true;
    this.orderedPizzas = this.orderedPizzas.filter((item) => {
      if (looking && item.id === pizza.id) {
        looking = false;
        this.totalAmount = this.totalAmount - pizza.price;
        return false;
      }

      return true;
    })
  }

  submitOrder(): Observable<Response> {
    let ids = [];
    for (let pizza of this.orderedPizzas) {
      ids.push(pizza.id);
    }

    return this.http.post<Response>(this.submitUrl, ids);
  }

  clearOrder(): void {
    this.orderedPizzas = [];
    this.totalAmount = 0;
  }

  getOrder(): Pizza[] {
    return this.orderedPizzas;
  }

  getOrderTotal(): string {
    var rounded = "$" + Math.round(this.totalAmount * 100) / 100 + "";
    var idx = rounded.indexOf('.');
    if (idx < 0) {
      rounded += ".00";
    } else if (idx + 2 == rounded.length) {
      rounded += "0";
    }

    return rounded;
  }

  constructor( private http: HttpClient ) { }
}
