import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { Subscription } from '../../../node_modules/rxjs';

const SESSION_KEY = "pizza42_session_registered";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  name: string;
  // new_session: string;
  // message: string;
  subscription: Subscription;

  ngOnInit() {
    this.subscription = this.authService.logInWatcher
        .subscribe((item) => {
      if (this.authService.isAuthenticated()) {
        this.authService.getProfile((err, profile) => {
          if (profile) {
            this.name = profile.given_name;
          }
        });
      } else {
        this.name = undefined;
        localStorage.removeItem(SESSION_KEY);
      }
    });
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  
  constructor(
    public authService: AuthService
  ) {  
  }
}
