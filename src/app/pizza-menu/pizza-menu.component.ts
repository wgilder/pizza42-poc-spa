import { Component, OnInit } from '@angular/core';
import { Pizza } from '../pizza'
import { PizzaService } from '../pizza.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-pizza-menu',
  templateUrl: './pizza-menu.component.html',
  styleUrls: ['./pizza-menu.component.css']
})
export class PizzaMenuComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private pizzaService: PizzaService
  ) { }

  pizzas: Pizza[];
  orderAmount: string;
  orderedPizzas: Pizza[] = [];
  error: string;
  success: string;

  clearOrder(): void {
    this.pizzaService.clearOrder();
    this.getOrder();
    this.getAmount();
  }

  submitOrder(): void {
    this.error = "";
    let ver = this.authService.userProfile['email_verified'] || false;
    if (!ver) {
      this.error = "You must first verify your email address!";
      return;
    }
  
    this.pizzaService.submitOrder().subscribe(resp => {
      this.success = "Congrats! You've successfully ordered pizzas! Now go ahead and order some more.";
      this.clearOrder();
    });
  }

  getPizzas(): void {
    this.pizzas = [];
    this.pizzaService.getPizzas().subscribe((obs) => { 
      this.pizzas = obs;
      // let results = JSON.parse(obs.text());
      // for (let key of results) {
      //   let pizza = new Pizza(key['id'], key['name'], key['toppings'], key['price']);
      //   this.pizzas.push(pizza);
      // }
    });
  }

  getOrder(): void {
    this.orderedPizzas = this.pizzaService.getOrder();
  }

  getAmount(): void {
    this.orderAmount = this.pizzaService.getOrderTotal();
  }

  addToOrder(pizza) {
    this.pizzaService.addToOrder(pizza);
    this.getAmount();
    this.getOrder();
    this.success = "";
    this.error = "";
  }

  ngOnInit() {
    this.getPizzas();
    this.getAmount();
  }
}
