import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CallbackComponent } from './callback/callback.component';
import { PizzaMenuComponent } from './pizza-menu/pizza-menu.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';


const routes: Routes = [
  { path: 'callback', component: CallbackComponent },
  { path: 'order', component: PizzaMenuComponent, canActivate: [ AuthGuard ] }, // menu should only be visible to authenticated users
  { path: 'home', component: HomeComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  providers: [ AuthGuard ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
