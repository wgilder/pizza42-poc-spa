import { environment } from '../environments/environment';

interface AuthConfig {
  clientID: string;
  domain: string;
  audience: string;
  callbackURL: string;
  apiURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'WH0zHIkx1uW4CSMtldoO59OpmVJfBFd1',
  domain: 'wgilder.eu.auth0.com',
  audience: 'http://pizza42-api.gildersleeve.eu',
  callbackURL: environment.callback_url,
  apiURL: environment.api_url,
};
