export class Pizza {
    constructor(id, name, toppings, price) {
        this.id = id;
        this.name = name;
        this.toppings = toppings;
        this.price = price;
    }

    id: string;
    name: string;
    toppings: string;
    price: number;
}